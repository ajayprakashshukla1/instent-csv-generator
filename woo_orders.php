<?php

/*
 */

function download_woocommerce_order() {


    if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) || !is_admin()) {
        return;
    }

    // Get an instance of the WC_Order object (same as before)

    $customer_orders = get_posts(array(
        'numberposts' => -1,
        'post_type' => wc_get_order_types(),
        'post_status' => array_keys(wc_get_order_statuses()),
    ));

    $csv_heading = array();
    if (count($customer_orders) != 0) {
        $heading = (Array) $customer_orders[0];
        $csv_heading_order = array_keys($heading);

        $order_meta_data = get_post_meta($customer_orders[0]->ID);
        $all_meta_for_order = array_map("remove_index", $order_meta_data);


        $csv_heading_order_meta = array_keys($all_meta_for_order);


        $csv_heading = array_merge($csv_heading_order, $csv_heading_order_meta);
        $csv_heading = array_map('csv_head_formate', $csv_heading);


        $order_data = array();
        $order_meta_data = array();





        foreach ($customer_orders as $order) {

            $order_meta_data = get_post_meta($order->ID);
            $all_meta_for_order = array_map("remove_index", $order_meta_data);

            $order_data_row = (Array) $order;

            $order_data = array_values($order_data_row);
            $order_meta_data = array_values($all_meta_for_order);
            $data[] = array_merge($order_data, $order_meta_data);
        }



        ob_get_clean();
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=Woocommerce_orders.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $file = fopen('php://output', 'w');
        fputcsv($file, $csv_heading);

        foreach ($data as $row) {
            fputcsv($file, $row);
        }
    }
}

function remove_index($value) {
    return $value[0];
}

function caps_firs_char_array($str) {

    return ucfirst($str);
}

function convert_head($text) {
    $text_array = explode('_', $text);
    $text_temp_array = array_map('caps_firs_char_array', $text_array);
    $final_text = implode(' ', $text_temp_array);

    return $final_text;
}

function csv_head_formate($head_value) {
    if (strpos($head_value, '_') !== false) {

        $head_value = convert_head($head_value);
    } else {

        $head_value = ucfirst($head_value);
    }
    return $head_value;
}
