<?php

/* 
 */

add_action('admin_footer', 'add_script_to_footer');

function add_script_to_footer() {
    $screen = get_current_screen();
    
    if ( $screen->id != "users" )   // Only add to users.php page
        return;
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
           
            $('#post-query-submit').after( '<input id="generate-csv-button" type="button" class="button" value="Generate Csv" name="" style="margin: 1px 8px 0 0;">' );
            $('#generate-csv-button').click(function(){
              
             var project_manager = $('#foa_order_custom_filter').val();
             var account_manager = $('#foa_order_custom_filter1').val();
             var graphic_designer =  $('#foa_order_custom_filter2').val();
             var has_term = $('#foa_order_custom_filter3').val();
             
             var date_from = $('.field-from').val();
             var date_to = $('.field-to').val();
            

             var url_parameter = '';
           
              if( project_manager != 'defaultvalue'){
                 url_parameter += '&project-managers='+project_manager;
               }
             if( account_manager != 'defaultvalue'){
                url_parameter += '&account-managers='+account_manager;
               }
               
              
              
              if(typeof value === "undefined"){
              
           }else{
               
                 if( graphic_designer != 'defaultvalue' ){
                url_parameter += '&graphic-designe='+graphic_designer; 
               }
               
             }
               if( has_term != 'defaultvalue'){
                 url_parameter += '&has-term='+has_term;
               }
               
               if( date_from != '' && date_to != '' ){
                 url_parameter += '&date_from='+date_from+'&date_to='+date_to;
               }
               
               
                window.location  = '<?php echo site_url(); ?>/wp-admin/admin.php?page=usrs_csv_download'+url_parameter;
            
             });
         
        });
    </script>
    <?php
}


add_action('admin_footer', 'add_order_script_to_footer');

function add_order_script_to_footer() {
    $screen = get_current_screen();
    
    if ( $screen->id != "edit-shop_order" )   // Only add to users.php page
        return; 
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
           
            $('#post-query-submit').after( '<input id="generate-csv-button" type="button" class="button" value="Generate Csv" name="" style="margin: -7px 8px 0 0 !important;">' );
            
         
            
            $('#generate-csv-button').click(function(){
            
            var project_manager = $('#foa_order_custom_filter').val();
             var account_manager = $('#foa_order_custom_filter1').val();
             var graphic_designer =  $('#foa_order_custom_filter2').val();
             var has_term = $('#foa_order_custom_filter3').val();
             
             var date_from = $('.field-from').val();
             var date_to = $('.field-to').val();
             
             var url_parameter = '';
           
              if( project_manager != 'defaultvalue' ){
                 url_parameter += '&Project-Manager='+project_manager;
               }
             if( account_manager != 'defaultvalue' ){
                url_parameter += '&Account-Manager='+account_manager;
               }
               
               if(typeof value === "undefined"){
              
           }else{
               
                 if( graphic_designer != 'defaultvalue' ){
                url_parameter += '&graphic-designe='+graphic_designer; 
               }
               
             }
               
               if( has_term != 'defaultvalue'){
                 url_parameter += '&has-term='+project_manager;
               }
               
                 if( date_from != '' && date_to != '' ){
                 url_parameter += '&date_from='+date_from+'&date_to='+date_to;
               }
               
               
                window.location  = '<?php echo site_url(); ?>/wp-admin/admin.php?page=orders_csv_download'+url_parameter;
            
             });
         
        });
    </script>
    <?php
}





// search query
function igc_get_user_based_on_filter( $query ) {
    
    global $pagenow,$wpdb; 	
     if ( is_admin() && isset($_GET['page']) && $_GET['page']=='usrs_csv_download' ) {	
         

		 $checkvalue = array();	
                 if(isset($_GET['has-term']) && $_GET['has-term'] != 'undefined' ){
		 $termvalue = $_GET['has-term'];
                 }
		 //$termvalue = !empty( $termvalue[ 0 ] ) ? $termvalue[ 0 ] : $termvalue[ 1 ];
		
                 if(isset($_GET['project-managers']) && $_GET['project-managers'] != 'undefined' ){
		 $projectmanagervalue = $_GET['project-managers'];
                 }
		// $projectmanagervalue = !empty( $projectmanagervalue[ 0 ] ) ? $projectmanagervalue[ 0 ] : $projectmanagervalue[ 1 ];
        
		 if(isset($_GET['account-managers'])&& $_GET['account-managers'] != 'undefined'){
		 $accountmangervalue1 = $_GET['account-managers'];
                 }
		  //$accountmangervalue1 = !empty( $accountmangervalue1[ 0 ] ) ? $accountmangervalue1[ 0 ] : $accountmangervalue1[ 1 ];
                if(isset($_GET['graphic-designer'])&& $_GET['graphic-designer'] != 'undefined'){
		 $accountmangervalue2 = $_GET['graphic-designer'];
                }
		 //$accountmangervalue2 = !empty( $accountmangervalue2[ 0 ] ) ? $accountmangervalue2[ 0 ] : $accountmangervalue2[ 1 ];
 		
		
		$prefix = $wpdb->prefix;
		$meta_name = "{$prefix}capabilities";
                
               
                
		if ($termvalue == 'yes' && isset( $_GET[ 'has-term' ] )  && $_GET['has-term']  != '') {
			  $checkvalue[] =  array(

                        'key' => 'has-term',

                        'value' => 'yes',  

						'compare' => '=='                     

                    );
		 }elseif( $termvalue != 'defaultvalue' && isset( $_GET[ 'has-term' ] )  && $_GET['has-term']  != '' ){ 
		  $checkvalue[] =  array(
                        'key' => 'has-term',
                         'value' => 'yes',
						'compare' => '!='                     
           			
                    );
		 } 		 
		if($projectmanagervalue != 'defaultvalue' && isset($_GET['project-managers']) && $_GET['project-managers'] != ''){
                    
			 $checkvalue[] =  array(
                        'key' => 'project-manager',
                        'value' => $projectmanagervalue, 
						'compare' => '=='  
                    );
		 }
		  if($accountmangervalue1 == 'accountnone' && isset($_GET['account-managers']) && $_GET['account-managers'] != ''){			
  			  $checkvalue[] =  array(
			            'relation' => 'OR', 
                        array(
                        'key' => 'account-manager',
						'value' => '', 
						'compare' => '=='  
		                 ),
						  array(
                        'key' => 'account-manager',
 						'compare' => 'NOT EXISTS'  
		                 )

                    );
		 }elseif($accountmangervalue1 != 'defaultvalue' && isset($_GET['account-managers']) && $_GET['account-managers'] != ''){
			 $checkvalue[] =  array(
                        'key' => 'account-manager',
                        'value' => $accountmangervalue1,
						'compare' => '==' 
                    );		 }
		 if($accountmangervalue2 == 'graphicnone' && isset($_GET['graphic-designer']) && $_GET['graphic-designer'] != ''){			
  			 $checkvalue[] =  array(
			            'relation' => 'OR', 
                        array(
                        'key' => 'graphic-design',
						'value' => '', 
						'compare' => '=='  
		                 ),
						  array(
                        'key' => 'graphic-design',
 						'compare' => 'NOT EXISTS'  
		                 )

                    );
		 }elseif($accountmangervalue2 != 'defaultvalue' && isset($_GET['graphic-designer']) && $_GET['graphic-designer'] != ''){
			 $checkvalue[] =  array(
                        'key' => 'graphic-design',
                        'value' => $accountmangervalue2, 
						'compare' => '=='  
                    );
		 }
		$available_drivers =   array( 
                array(	
                        'key' => "{$meta_name}",
                        'value' => 'customer',
                        'compare' => 'LIKE' ),
                        $checkvalue,				
				   );	
                       
                 
 		$query->set( 'meta_query', $available_drivers );
                
               
              
                
                
     }
                
               
                 return $query;
}
add_filter( 'pre_get_users', 'igc_get_user_based_on_filter'  ,10,1); 




   function icf_product_filter_where( $query ) {
		global $pagenow;
        $type = 'post';
        
        
        
    if (isset($_GET['post_type'])) {
       $type = $_GET['post_type'];
    }

    if ( is_admin() && isset($_GET['page']) && $_GET['page']=='orders_csv_download' ) {	
        

        $query->query_vars['meta_key'] = '_customer_user';

 		$usersid = array();
		$checkvalue = array();
                
                if(isset($_GET['Project-Manager']) && $_GET['Project-Manager'] !='undefined'){
		 $projectmanagervalue = $_GET['Project-Manager'];
                }
                
                if(isset($_GET['Account-Manager']) && $_GET['Account-Manager'] !='undefined'){
		 $accountmangervalue1 = $_GET['Account-Manager'];
                }
                
                if(isset($_GET['graphic-designe']) && $_GET['graphic-designe'] !='undefined'){ 
		 $accountmangervalue2 = $_GET['graphic-designe'];
                }
                
                
		 if(isset($projectmanagervalue) && isset($_GET['Project-Manager']) && $_GET['Project-Manager'] != ''){
                     

			 $checkvalue[] =  array(

                        'key' => 'project-manager',

                        'value' => $projectmanagervalue,  

						'compare' => '=='                     

                    );

		 }
                 
  		  if(isset($accountmangervalue1) && $accountmangervalue1 == 'accountnone' && isset($_GET['Account-Manager']) && $_GET['Account-Manager'] != ''){
                      
                       
  			  $checkvalue[] =  array(
			            'relation' => 'OR', 
                        array(
                        'key' => 'account-manager',
						'value' => '', 
						'compare' => '=='  
		                 ),
						  array(
                        'key' => 'account-manager',
 						'compare' => 'NOT EXISTS'  
		                 )

                    );
		 }elseif(isset($accountmangervalue1) && isset($_GET['Account-Manager']) && $_GET['Account-Manager'] != ''){
                     
                     
                     
			 $checkvalue[] = array(
                        'key' => 'account-manager',
                        'value' => $accountmangervalue1,  
						'compare' => '==' 
                    );
		 }	
		 if(isset($accountmangervalue2) && isset($_GET['graphic-designe']) && $_GET['graphic-designe'] != ''){

			 $checkvalue[] =  array(

                        'key' => 'graphic_design1',

                        'value' => $accountmangervalue2,  

						'compare' => '=='                     

                    );

		 }

		
	
		$available_drivers = get_users(
            array(
                'role' => 'customer',
                'meta_query' => $checkvalue
            		)

       			 );
	
                
            
                
	if(!empty($available_drivers)){		
		 foreach($available_drivers as $available_driver){
			 $usersid[] = $available_driver->ID;
		 }
			$query->query_vars['meta_value'] = $usersid;
	}else{		
		$query->query_vars['meta_value'] = 'acxzhj';
	}
	
        
        if(isset($_GET['date_from'])&& $_GET['date_from'] != '' && isset($_GET['date_to']) && $_GET['date_to'] != ''){
                
                    
                $from = explode( '-', sanitize_text_field( $_GET['date_from'] ) );
			$to   = explode( '-', sanitize_text_field( $_GET['date_to'] ) );
			$from = array_map( 'intval', $from );
			$to   = array_map( 'intval', $to );
                       
                          
                        
			if (3 === count( $to ) && 3 === count( $from )) {
				list( $year_from, $month_from, $day_from ) = $from;
				list( $year_to, $month_to, $day_to )       = $to;
			} else {
				return $query;
			}

                         
                        
			$query->set(
				'date_query',
				array(
					'after' => array(
						'year'  => $year_from,
						'month' => $month_from,
						'day'   => $day_from,
					),
					'before' => array(
						'year'  => $year_to,
						'month' => $month_to,
						'day'   => $day_to,
					),
					'inclusive' => apply_filters( 'woo_orders_filterby_date_range_query_is_inclusive', true ),
					'column'    => apply_filters( 'woo_orders_filterby_date_query_column', 'post_date' ),				)
			);
                
                        
                    
     
		 }
      
       
        
    }	
 
    
        }               
                
        add_filter('pre_get_posts','icf_product_filter_where');          






function igc_extended_user_search( $user_query ){	 
 	global $pagenow, $wpdb;
        
        
        
        
  if ( is_admin() && isset($_GET['page']) && $_GET['page'] == 'usrs_csv_download' && isset($_GET['date_from']) && $_GET['date_from'] !='' && isset($_GET['date_to'])  && $_GET['date_to'] != '' ) {
  
      
		 $date  = $_GET['date_from'];
		 $date1  = $_GET['date_to']; 
    	 

		 $year  = substr( $date, 0, -2 );
		 $month = substr( $date, -2, 5 );
		 $year1  = substr( $date1, 0, -2 );
		 $month1 = substr( $date1, -2, 5 );
 		
		if($date>0 && $date1 >0 )
		 $user_query->query_where .= " AND user_registered >= '" . $date . " 00:00:00' AND user_registered <= '" . $date1 . " 23:59:59'"; 

  }
      
                return $user_query;
  
}
add_action( 'pre_user_query', 'igc_extended_user_search' ,9,1); 


function remove_menus(){
    
   echo '<style type="text/css"> #toplevel_page_usrs_csv_download{display:none !important} #toplevel_page_orders_csv_download{display:none !important} </style>';
}


add_action( 'admin_head', 'remove_menus' );