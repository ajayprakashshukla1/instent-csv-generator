<?php

/*
 */

function download_userlist_csv() {
    ob_get_clean();
    $users = get_users();

    $csv_heading = array();
    if (count($users) != 0) {
        $heading = (Array) $users[0]->data;
        unset($heading['user_pass']);
        unset($heading['user_activation_key']);
        $csv_heading_user = array_keys($heading);
        $user_meta_data_head = get_user_meta($users[0]->ID);

        $all_meta_for_user = array_map("remove_index", $user_meta_data_head);

        unset($all_meta_for_user['session_tokens']);
        $csv_heading_user_meta = array_keys($all_meta_for_user);

        $csv_heading_user[] = 'roles';
        $csv_heading = array_merge($csv_heading_user, $csv_heading_user_meta);
        $csv_heading = array_map('csv_head_formate', $csv_heading);
        $user_data = array();
        $user_meta_data = array();

        foreach ($users as $user) {

            $user_meta_data = get_user_meta($user->ID);
            $all_meta_for_user = array_map("remove_index", $user_meta_data);
			
	        $project_manger_id = $all_meta_for_user['project-manager'];
			$account_manager_id = $all_meta_for_user['account-manager'];
			/*
			$project_manager_first_name = get_user_meta( $project_manger_id,'first_name',true);
			$project_manager_last_name = get_user_meta( $project_manger_id,'last_name',true);
            $project_manager_name =  $project_manager_first_name.' '.$project_manager_last_name;
			
			$account_manager_first_name = get_user_meta( $account_manager_id,'first_name',true );
			$account_manager_last_name = get_user_meta(  $account_manager_id,'last_name',true );
            $account_manager_name =  $account_manager_first_name.' '.$account_manager_last_name;
			*/
		   
		  $project_manager = get_user_by( 'ID', $project_manger_id );
          $project_manager_name = $project_manager->display_name;
		  
		  $account_manager = get_user_by( 'ID', $account_manager_id );
          $account_manager_name = $account_manager->display_name;
		 
		  
		  $all_meta_for_user['project-manager'] = $project_manager_name;
          $all_meta_for_user['account-manager'] = $account_manager_name;
			
            $user_data_row = (Array) $user->data;
			
            $user_data_row['roles'] = implode(',', $user->roles);

            unset($user_data_row['user_pass']);
            unset($user_data_row['user_activation_key']);

            $user_data = array_values($user_data_row);
			
            $user_meta_data = array_values($all_meta_for_user);
			
			
			
			
            $data[] = array_merge($user_data, $user_meta_data);
        }

       

        ob_get_clean();
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=Userlist.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $file = fopen('php://output', 'w');
        fputcsv($file, $csv_heading);

        foreach ($data as $row) {
            fputcsv($file, $row);
        }
    }
    
}

function remove_index($value) {
    return $value[0];
}

function caps_firs_char_array($str) {

    return ucfirst($str);
}

function convert_head($text) {
    $text_array = explode('_', $text);
    $text_temp_array = array_map('caps_firs_char_array', $text_array);
    $final_text = implode(' ', $text_temp_array);

    return $final_text;
}

function csv_head_formate($head_value) {
    if (strpos($head_value, '_') !== false) {

        $head_value = convert_head($head_value);
    } else {

        $head_value = ucfirst($head_value);
    }
    return $head_value;
}
