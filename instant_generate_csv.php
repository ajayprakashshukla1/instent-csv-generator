<?php

/*
 *
 * Plugin Name: Instent CSV Generator
 * Description: This plugin is for Generating CSV with Data on User and Wocommerce Order Screen.
 * Plugin URI: http://codup.io/
 * Version: 1.1.1.1
 * Author: codup
 * Author URI: http://codup.io/
 * Text Domain: woocommerce-filter-custom
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */
require_once( plugin_dir_path(__FILE__) . 'hooks.php' );

class Generatecsv {

   

    public static function generate_woo_orders_csv_request($atts) {
        require_once( plugin_dir_path(__FILE__) . 'woo_orders.php' );
        download_woocommerce_order();

        exit;
    }

    function generate_usrlist_csv_request() {

        require_once( plugin_dir_path(__FILE__) . 'filtered_user.php' );
        download_userlist_csv();
        exit();
    }

}

function my_menu() {
    add_menu_page('Generate CSV', 'Generate CSV', 'generate_csv', 'usrs_csv_download', array('Generatecsv', 'generate_usrlist_csv_request'));
    add_menu_page('Generate CSV', 'Generate CSV', 'generate_csv', 'orders_csv_download', array('Generatecsv', 'generate_woo_orders_csv_request'));
}

add_action('admin_menu', 'my_menu');




